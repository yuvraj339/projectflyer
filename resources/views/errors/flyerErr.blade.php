@if ($errors->any())

    <ul class="alert alert danger">

        @foreach ($errors->all() as $errregi)
            <li class="alert alert-danger"> {{ $errregi }}</li>
        @endforeach

    </ul>

@endif