@extends('layout')

@section('content')

<div class="jumbotron">
	<h2>Project flyer</h2>
	<p>
		This is a template showcasing the optional theme stylesheet included in
		 Bootstrap. Use it as a starting point to create something more unique by
	  	building on or modifying it.
	</p>
	@if($signedIn)
	<a href="{{ url('flyer/create') }}" class="btn btn-primary">Create flyers</a>
		@else
		<a href="{{ url('auth/login') }}" class="btn btn-primary">Login/Regiser</a>

	@endif
</div>

@stop