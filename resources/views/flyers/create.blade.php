@extends('layout')

@section('content')
    <div class="jumbotron col-lg-12">
        <legend class="control-label selling" for="focusedInput">Selling your home ?</legend>

        <div class="col-lg-8">

            <form class="col-lg-12 form-group" method="post" enctype="multipart/form-data" action="{{url('flyer')}}">
                @include('flyers.form')
            </form>
        </div>
        <div class="col-lg-4">
            @include('errors.flyerErr')

        </div>
    </div>
@stop