@inject('countrise','App\Http\Utilities\Country')
{{ csrf_field() }}
<div class="form-group col-lg-10">
    <input type="text" placeholder="Street" id="street" name="street" class="form-control" required>
    <!-- /input-group -->
</div>
<div class="form-group col-lg-10">
    <input type="text" placeholder="City" id="city" name="city" class="form-control" required>
    <!-- /input-group -->
</div>

<div class="form-group col-lg-10">
    <input type="text" placeholder="Postal Zip" id="zip" name="zip" class="form-control" required>
    <!-- /input-group -->
</div>
<div class="form-group col-lg-10">
    {{--<label class="col-lg-2 control-label" for="select">Selects</label>--}}
    <select id="country" class="form-control" name="country">
        @foreach($countrise::all() as $country)
            <option value="{{ $country }}">{{ $country }}</option>
        @endforeach
    </select>
</div>
<div class="form-group col-lg-10">
    <input type="text" placeholder="State" id="state" name="state" class="form-control" required>
    <!-- /input-group -->
</div>
<hr>
<div class="form-group col-lg-10">
    <input type="text" placeholder="Sale Price" id="price" name="price" class="form-control" required>
    <!-- /input-group -->
</div>
<div class="form-group">
    {{--<label for="textArea" class="col-lg-2 control-label">Textarea</label>--}}
    <div class="col-lg-10">
        <span class="help-block">Home description</span>

        <textarea class="form-control" name="description" rows="3" id="description"></textarea>
    </div>
</div>
{{--<div class="form-group col-lg-10">--}}
    {{--<input type="file" placeholder="select photo" id="photos" name="photos" class="form-control">--}}
    {{--<!-- /input-group -->--}}
{{--</div>--}}
<br />
<div class="form-group col-lg-10">
    <button class="btn btn-primary">Create flyers</button>
</div>
