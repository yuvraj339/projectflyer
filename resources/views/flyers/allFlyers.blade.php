@extends('layout')
@section('content')
    @inject('flyers','\App\Flyer')

    <div class="jumbotron">
        <p class="bg-danger">All "FLYERS" of registered users  => <a href="{{ url('flyer/create')  }}">  Create a new flyer</a></p>
        <hr/>
        <p>
            This is a template showcasing the optional theme stylesheet included in
            Bootstrap. Use it as a starting point to create something more unique by
            building on or modifying it.
        </p>
        <hr/>
        <div>
            @foreach($flyers::allFlyersDetails() as $flyers)
               <div><a href="{{ flyer_path($flyers) }}"><p>{{ $flyers->street }}</p></a> </div>
               <div><span>Price : {{ $flyers->price }}</span></div>
                <div><span class="text-justify text-muted">{{ $flyers->description }}</span></div>
                <hr>
            @endforeach
        </div>
    </div>
@stop