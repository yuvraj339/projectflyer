@extends('layout')
@section('content')
    <div class="row">
        <div class="container">
            <div class="col-lg-4">
                <h1>{{ $flyer->street }}</h1>
                <h4>{{ $flyer->price }}</h4>
                <hr>
                <span class="text-justify">{{ $flyer->description }}</span>
            </div>

            <div class="col-lg-8">
                <div class="image_position">
                    @foreach($flyer->photos->chunk(6)as $photo_set)
                        <div class="row">
                            @foreach($photo_set as $photo_sets)
                                <div class="col-sm-2">
                                    <img src="{{url($photo_sets->thumbnail_path)}}" class="img img-thumbnail">
                                    &nbsp;
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
                <div id="owl-demo" class="owl-carousel">
                    @foreach($flyer->photos->chunk(1) as $set)
                        <div class="row">
                            @foreach($set as $photo)
                                {{--<div class="col-md-3">--}}
                                {{--<img class="thumbnail img-thumbnail imgSlide" src="{{ url($photo->thumbnail_path) }}"--}}
                                {{--alt="">--}}
                                {{--</div>--}}
                                <div class="item"><img src="{{ url($photo->path) }}"
                                                       alt="The Last of us"></div>
                            @endforeach
                        </div>
                    @endforeach

                </div>

                @if($user && $user->owns($flyer))
                    <hr>
                    <div class="col-md-12">
                        <form method="post"
                              id="addPhotosForm"
                              class="dropzone"
                              action="{{ url($flyer->zip."/".$flyer->street.'/photos') }}">
                            {{ csrf_field() }}
                        </form>
                        <h3>Add photos for flyer</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('flyer.script')
    <style>
        #owl-demo .item img {
            display: block;
            width: 100%;
            autoHeight : true;
        }
    </style>


    <script>
        $(document).ready(function () {
            $("#owl-demo").owlCarousel({
                autoPlay: 3000,
                navigation: true,
                slideSpeed: 300,
                paginationSpeed: 400,
                singleItem: true
            });
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.js"></script>
    <script>
        Dropzone.options.addPhotosForm = {
            paramName: 'photo',
            maxFileSize: '4',
            acceptedFiles: '.jpg, .jpeg, .png, .gif'
        }
    </script>


@stop