<!DOCTYPE html>

<html>
<head>
    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('css/style.css') !!}
    {!! Html::style('css/sweetalert.css') !!}
    {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.css') !!}
    {!! Html::style('css/owl.carousel.css') !!}
    {!! Html::style('css/owl.transitions.css') !!}
    {!! Html::style('css/owl.theme.css') !!}
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            {{--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"--}}
                    {{--aria-expanded="false" aria-controls="navbar">--}}
                {{--<span class="sr-only">Toggle navigation</span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--<span class="icon-bar"></span>--}}
            {{--</button>--}}
            <a class="navbar-brand" href="#">ProjectFlyer</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{url('allflyers')}}">All flyer</a></li>
                <li><a href="#contact">Contact</a></li>
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"--}}
                       {{--aria-expanded="false">Dropdown <span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="#">Action</a></li>--}}
                        {{--<li><a href="#">Another action</a></li>--}}
                        {{--<li><a href="#">Something else here</a></li>--}}
                        {{--<li role="separator" class="divider"></li>--}}
                        {{--<li class="dropdown-header">Nav header</li>--}}
                        {{--<li><a href="#">Separated link</a></li>--}}
                        {{--<li><a href="#">One more separated link</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>
            @if($signedIn)
               <p class="navbar-text navbar-right">Hello  {{ $user->name }}
               <a href="{{url('auth/logout')}}" class="bg-danger ">Logout </a>
                </p>
            @endif
        </div>

        <!--/.nav-collapse -->
    </div>
</nav>
<div class="container">
    @yield('content')
</div>
</body>
<div style="height: 20px">
    &nbsp;
</div>
<footer >
    <div class="container bottom">
        <div class="row">
            {{--<div class="col-lg-4"></div>--}}
            <div class="col-lg-8 col-lg-offset-4 ">
                <ul class="list-inline">
                    <li>
                        <a href="#">Home</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#about">About</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#services">Services</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#contact">Contact</a>
                    </li>
                </ul>
                <p class="copyright text-muted small">Copyright &copy; Project Flyer 2015. All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>
{!! HTML::script('js/jquery.min.js') !!}
{!! HTML::script('js/owl.carousel.js') !!}
{!! HTML::script('js/sweetalert.min.js') !!}
@include('flash')
@yield('flyer.script')
</html>
