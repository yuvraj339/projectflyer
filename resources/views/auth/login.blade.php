@extends('layout')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="well bs-component">

            {!! Form::open(array('class' => 'form-horizontal')) !!}

            <fieldset>
                <legend>SignIn</legend>
                <div class="form-group">
                    <label for="inputEmail" class="col-lg-2 control-label">Email</label>

                    <div class="col-lg-10">
                        <input class="form-control" id="inputEmail" name="email" placeholder="Email" type="email" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword" class="col-lg-2 control-label">Password</label>

                    <div class="col-lg-10">
                        <input class="form-control" id="inputPassword" name="password" placeholder="Password" required type="password">

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"> Remember me
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="reset" class="btn btn-default">Clear</button>
                        <button type="submit" class="btn btn-primary" name="signin">SignIn</button>
                        <br><br>
                        <a href="{{ url('auth/register') }}">SignUp</a><br>
                        <a href="{{ url('forgotpassword') }}">Forgot your Password</a><br>

                    </div>
                </div>
            </fieldset>
            {!! Form::close() !!}

            @include('errors.flyerErr')

        </div>
    </div>


@stop