<div class="col-lg-6">{!! Form::text('firstName', null, ['class'=>'form-control', 'placeholder'=>'Enter First Name']) !!}</div>
<div class="col-lg-6"> {!! Form::text('lastName', null, ['class'=>'form-control', 'placeholder'=>'Enter Last Name']) !!}</div>
<br/>

<div class="col-lg-12"> {!! Form::text('email',null, ['class'=>'form-control', 'placeholder'=>'Enter Email']) !!}</div><br />
<div class="col-lg-12"><span class="text-muted">How many apps do you currently have in the app store ?</span>
    {!! Form::selectRange('howManyApps', 1, 30,null,['class'=>'form-control' ]) !!}</div>
<br/>

<div class="col-lg-12">
    <span class="text-muted">Password (leave blank if you don't want to change it)</span><br />
    {!! Form::password('password',['placeholder'=>'Enter Password','class'=>'form-control']) !!}</div>
<br/>

<div class="col-lg-12">
    {!! Form::password('password_confirmation',['placeholder'=>'Confirm  Password','class'=>'form-control']) !!}</div>
<br/>
<div class="col-md-12"><br /></div>
<div class="col-md-12 pull-right"> {!! Form::submit($btnName, ['class'=>'btn btn-primary btn-lg']) !!}  </div>
