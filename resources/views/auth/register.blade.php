@extends('layout')

@section('content')

    @include('errors.flyerErr')

    <div class="row">
        <div class="col-lg-12">
            <div class=" col-lg-8 col-lg-offset-2">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li class="active">Registration : New User</li>

                </ul>
                {{--<h5 ></h5>--}}
                {!! Form::open() !!}
                <div>{!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Enter your name', 'required']) !!}</div>
                <br/>

                <div> {!! Form::text('email',null, ['class'=>'form-control', 'placeholder'=>'Enter email', 'required']) !!}</div>
                <br/>

                <div>{!! Form::password('password',['placeholder'=>'Enter password','class'=>'form-control', 'required']) !!}</div>
                <br/>

                <div>
                    {!! Form::password('password_confirmation',['placeholder'=>'Confirm  password','class'=>'form-control', 'required']) !!}</div>
                <br/>

                <div class="pull-left"> {!! Form::submit('submit', ['class'=>'btn btn-primary btn-md']) !!}  </div>


                {!! Form::close() !!}
                {{--<a href="/auth/login" class="btn btn-primary btn-lg">Login</a>--}}
            </div>
        </div>
    </div>
@stop
