<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flyer extends Model
{
    protected $fillable = [
        'street',
        'user_id',
        'city',
        'zip',
        'state',
        'country',
        'price',
        'price',
        'description'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {

        return $this->hasMany('App\FlyerPhoto');
    }
//    public function photos1($data)
//    {
//        dd($this->hasMany('App\FlyerPhoto'));
//
//        return $this->hasMany('App\FlyerPhoto')->chunk($data);
//    }

    /**
     * @param $zip
     * @param $street
     * @return mixed
     */
    public static function locatedAt($zip, $street)
    {
        $street = str_replace('-', ' ', $street);
       return static::where(compact('zip','street'))->firstOrFail();
    }

    public static function allFlyersDetails()
    {
        return static::all();
    }


    /**
     * @param $price
     * @return string
     */
    public function getPriceAttribute($price)
    {
        return  number_format($price) . ' INR ';
    }

    /**
     * @param FlyerPhoto $photo
     * @return Model
     */
    public function addPhoto($photo)
    {
        return $this->photos()->save($photo);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    /**
     * @param User $user
     */
    public function ownedBy(User $user)
    {
        $this->user_id == $user->id;
    }
}
