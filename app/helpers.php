<?php
/**
 * Created by PhpStorm.
 * User: user121
 * Date: 9/23/2015
 * Time: 5:57 PM
 */

function flash($title = null ,$message = null)
{
$flash = app('App\Http\Flash');
    if(func_num_args()== 0)
    {
        return $flash;
    }
    return $flash->info($title, $message);
}

/**
 * A path to given flyer
 * @param \App\Flyer $flyer
 * @return string
 */
function flyer_path(\App\Flyer $flyer)
{
    return $flyer->zip .'/'.str_replace('-',' ',$flyer->street) ;
}