<?php

namespace App\Http\Controllers;

use App\Flyer;
use App\FlyerPhoto;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\FlyerRequest;
use App\Http\Requests\AddPhotoRequest;
use App\Http\Controllers\Traits\AuthorizesUsers;

class FlyersController extends Controller
{
use AuthorizesUsers;
    public function __construct()
    {
        $this->middleware('auth',['except' => 'show']);
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        flash()->overlay('hello World','information');
        return view('flyers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FlyerRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FlyerRequest $request)
    {

        $flyer = $this->user->published(
            new Flyer($request->all())
        );
        //save into database
        flash()->success('Data saved','success');
        return redirect(flyer_path($flyer));
        //return redirect
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($zip , $street)
    {
      $flyer = Flyer::locatedAt($zip , $street);
        return view('flyers.show',compact('flyer'));
    }

    public function flyerPhotos($zip , $street, AddPhotoRequest $request)
    {
        /**
         * this method for set validation on same page without making a separate  request
         */


//        $this->validate($request, [
//            'photo' => 'required|mimes:jpg,jpeg,gif,png'
//        ]);
//        $file = $request->file('photo');

        /**
         * this method for set authentication for upload photo
         */

//        if(! $this->userCreatedFlyer($request))
//        {
//            return $this->unauthorized($request);
//        }

        /**
         * @function makePhoto() create a request for upload photo and set names and move into folder with
         *  thumbnail's and original photo
         */

//        $photo = $this->makePhoto($request->file('photo'));
        $photo = FlyerPhoto::fromFile($request->file('photo'));
        Flyer::locatedAt($zip , $street)->addPhoto($photo);
//        $flyer->photos()->create(['path'=>"flyers/photos/{$fileName}"]);

    }

    public function allFlyers()
    {
//        $allFlyers = Flyer::allFlyersDetails();
//        this->allFlyers()->street' , 'this->allFlyers()->description
        return view('flyers.allFlyers');
    }

//    public function makePhoto(UploadedFile $file)
//    {
//        $photo = FlyerPhoto::named($file->getClientOriginalName());
//        $photo->move($file);
////        $photo = FlyerPhoto::named($file->getClientOriginalName())
////                ->move($file);
//        return $photo;
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
