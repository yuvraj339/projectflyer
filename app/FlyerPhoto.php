<?php

namespace App;
use Image;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FlyerPhoto extends Model
{
    protected static function boot()
    {
        static::creating(function($photo) {
            return $photo->upload();
        });
    }
//    protected $baseDir = 'flyers/photos';
    protected $fillable = ['path','name','thumbnail_path'];
    protected $file;
    public function flyer()
    {
        return $this->belongsTo('App\Flyer');
    }

    public static function fromFile(UploadedFile $file)
    {
        $photo = new static;
        $photo->file = $file;
        return  $photo->fill([
           'name' => $photo->fileName(),
           'path' => $photo->filePath(),
           'thumbnail_path' => $photo->thumbnailPath(),
        ]);
    }

//    /**
//     * @param $name
//     * @return mixed
//     */
//    public static function named($name)
//    {
//
//        return (new static)->saveAs($name);
////        $photo = new static;
////        $fileName = time().$file->getClientOriginalName();
////        $photo->path = $photo->baseDir . '/' . $fileName;
////        $file->move($photo->baseDir, $fileName);
////        return $photo;
//
//    }

//    /**
//     * @param $name
//     * @return $this
//     */
//    public function saveAs($name)
//    {
//
//        $this->name = sprintf('%s-%s',time(),$name);
//        $this->path = sprintf('%s/%s',$this->baseDir,$this->name);
//        $this->thumbnail_path = sprintf('%s/tn-%s',$this->baseDir,$this->name);
//        return $this;
//    }
//
//    /**
//     * @param UploadedFile $file
//     */
//    public function move(UploadedFile $file)
//    {
//        $file->move($this->baseDir,$this->name);
//        $this->makeThumbnail();
//    }

    public function upload()
    {
        $this->file->move($this->baseDir(),$this->fileName());
        $this->makeThumbnail();
    }
    /**
     *
     */
    public function makeThumbnail()
    {
        Image::make($this->filePath())
            ->fit(100)
            ->save($this->thumbnail_path);
    }

    private function baseDir()
    {
        return 'flyers/photos';
    }

    private function filePath()
    {
        return $this->baseDir() . '/' . $this->fileName();
    }

    private function thumbnailPath()
    {
        return $this->baseDir() . '/tn-' . $this->fileName();
    }
    private function fileName()
    {
        $name = md5(
            time() . $this->file->getClientOriginalName()
        );
        $extension = $this->file->getClientOriginalExtension();

        return "{$name}.{$extension}";
    }


}
